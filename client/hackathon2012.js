//set up the collections
var Games = new Meteor.Collection('games'),
    Challenges = new Meteor.Collection('challenges'),
    Users = new Meteor.Collection('users'),
    Groups = new Meteor.Collection('groups');




var game = false ;
if (Session.get('gameId')) {

	game = Games.find({'_id': Session.get('gameId')});
}

Template.main.inGame = function() {
	return Session.get('inGame') || false;
}

Template.main.showRegister = function() {
	return Session.get('register') ||false;
}

Template.main.userLoggedIn = function(){
	return Session.get('userId') || false;
}

Template.scoreboard.games = function() {
	return Games.find({private: false});
};

Template.userList.users = function() {
	return Users.find({'_id':{'$ne': Session.get('userId')}});
}


Template.userList.events = {
	'click .challenge': function(event) {
		var userId = $(event.target).data('id');
		var toUser = Users.findOne({_id : userId}),
				fromUser = Users.findOne({_id : Session.get('userId')});
		Challenges.insert({
			from: {
				_id: fromUser._id,
				name: fromUser.name,
				username: fromUser.username
			},
			to: {
				_id: toUser._id,
				name: toUser.name,
				username: toUser.username
			},
			sent: new Date().getTime()
		});
		$(event.target).addClass('disabled');
		return false;
	}
}

Template.challengeList.challenges = function() {
	var challenges = Challenges.find({'accepted': {$exists: false}, 'to._id' : Session.get('userId')});
	return challenges;
}

Template.challengeList.events = {
	'click .approve': function(event) {
		var chId = $(event.target).data('id');
		var challenge = Challenges.findOne({'_id': chId});
		var team1User = Users.findOne({'_id': challenge.to._id});
		var team1 = { points: 0, users: [{name: team1User.name, username: team1User.username, _id: team1User._id}]};
		var team2User = Users.findOne({'_id': challenge.from._id});
		var team2 = { points: 0, users: [{name: team2User.name, username: team2User.username, _id: team2User._id}]};
		var game = {team1: team1, team2: team2, scoreHistory:[], startTime: new Date().getTime(), winner:-1, gameStatus: 'A'};
		var gameId = Games.insert(game);

		Challenges.update({'_id': chId}, {'$set' : {accepted: true, responseDate: new Date().getTime(), gameId: gameId}});
		Session.set('inGame', true);
		return false;
	},
	'click .deny': function(event) {
		var chId = $(event.target).data('id');
		Challenges.update({'_id': chId}, {'$set' : {accepted: false,responseDate: new Date().getTime()}});
		return false;
	}
};

Template.games.game = function() {
	console.log(Session.get('userId') + " userid");
	var game = Games.findOne({'team1.users.0._id': Session.get('userId')});
	console.log(Session.get('userId') + " userid");
	if (game) {
		return game;
	}
	var game = Games.findOne({'team2.users.0._id':Session.get('userId')});
	return game;
};

Template.games.events = {
	'click .score' : function() {

	}
};



Template.loginPage.events = {
	'click .toRegister' : function(e){
		Session.set('register', true);
	},
	'submit form' : function(e) {
		e.stopPropagation();
		e.preventDefault();

		var handle = $('#username').val(),
				passwd = $('#passwd').val();
		var user = Users.findOne({username: handle, password: passwd});
		if (user) {
			Session.set('userId', user._id);
		} else {
			alert('Your credentials do not match up');
		}
		return false;
	}
};
Template.registration.events = {
	'click .toSignin' : function(e) {
		Session.set('register', false);
	},
	'submit form' : function(e) {
		e.stopPropagation();
		e.preventDefault();
		var handle = $('#username').val(),
				realName = $('#realName').val();
				passwd = $('#passwd').val();
		if (handle == "" || realName == "" || passwd == "") {
			alert('all fields are required');
			return false;
		} else if (Users.findOne({username: handle})) {
			alert('There is already a user!!!');
			console.log(Users.findOne({username: handle}));
			return false;
		} else {
			var user_id = Users.insert({username: handle, name: realName, password: passwd});
			if (!user_id) { alert("there was a problem, please try again"); return false;  }
			Session.set('userId', user_id);
		}
		return false;
	}
};

/*
Template.hello.greeting = function () {
	return "Welcome to hackathon2012.";
};

Template.hello.events = {
	'click input' : function () {
		// template data, if any, is available in 'this'
		if (typeof console !== 'undefined')
			console.log("You pressed the button");
	}
};
*/
